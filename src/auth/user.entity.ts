import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Task } from '../tasks/task.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  // if eager is true, whenever we fetch the users, the tasks will be fetched too
  @OneToMany((_type) => Task, (task) => task.user, { eager: true })
  tasks: Task[];
}
