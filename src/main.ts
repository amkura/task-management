import { Logger, ValidationPipe } from "@nestjs/common";
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { TransformInterceptor } from './transform.interceptor';

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe()); // whenever validation pipes are now encountered, it will execute them
  app.useGlobalInterceptors(new TransformInterceptor());
  await app.listen(3000);
  logger.log(`App listening on port 3000`);
}
bootstrap();
